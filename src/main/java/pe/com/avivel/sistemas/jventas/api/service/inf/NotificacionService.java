package pe.com.avivel.sistemas.jventas.api.service.inf;

import pe.com.avivel.sistemas.jventas.api.entity.Notificacion;
import pe.com.avivel.sistemas.jventas.api.entity.NotificacionEstado;
import pe.com.avivel.sistemas.jventas.api.entity.NotificacionStatus;
import pe.com.avivel.sistemas.jventas.api.service.exception.ServiceException;
import pe.com.avivel.sistemas.jventas.api.service.generic.GenericService;

import java.util.Date;
import java.util.List;

public interface NotificacionService extends GenericService<Notificacion> {

    List<Notificacion> getNotificacionesByEstado(NotificacionEstado notificacionEstado);

    int saveNotificacionReporte(Integer notificacionId, Integer reporteId) throws ServiceException;

    int deleteNotificacionReporte(Integer notificacionId, Integer reporteId) throws ServiceException;

    boolean updateCantidades(Integer cantidadEnviar, Integer cantidadEnviados, Integer notificacionId);

    boolean updateFechasEjecuciones(Date nuevaUltimaEjecucion, Date nuevaProximaEjecucion, Integer notificacionId);

    void updateStatus(NotificacionStatus notificacionStatus, Integer notificacionId);

    boolean sendNotificacion(Integer notificacionId) throws ServiceException;
}
