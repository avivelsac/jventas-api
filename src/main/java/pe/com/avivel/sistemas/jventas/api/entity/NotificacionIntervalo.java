package pe.com.avivel.sistemas.jventas.api.entity;

import java.util.Arrays;
import java.util.List;

public enum NotificacionIntervalo {

    DIA("DIA"),
    SEMANA("SEMANA");

    private final String intervalo;

    NotificacionIntervalo(String intervalo) {
        this.intervalo = intervalo;
    }

    public static List<NotificacionIntervalo> getAll() {
        return Arrays.asList(NotificacionIntervalo.values());
    }

    @Override
    public String toString() {
        return intervalo;
    }
}
