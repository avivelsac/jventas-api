package pe.com.avivel.sistemas.jventas.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity(name = "ReporteJasperParam")
@Table(name = "reporte_jasper_param")
public class ReporteJasperParam implements Serializable {

    @Id
    @Column(name = "reporte_jasper_param_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer reporteJasperParamId;

    @Column(name = "reporte_jasper_param_name")
    private String reporteJasperParamName;

    @Column(name = "reporte_jasper_param_info")
    private String reporteJasperParamInfo;

    @JoinColumn(name = "reporte_jasper_id", referencedColumnName = "reporte_jasper_id")
    @ManyToOne(optional = false, targetEntity = ReporteJasper.class)
    @JsonIgnore
    private ReporteJasper reporteJasper;
}
