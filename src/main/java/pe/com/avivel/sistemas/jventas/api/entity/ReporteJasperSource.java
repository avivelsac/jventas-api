package pe.com.avivel.sistemas.jventas.api.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@ToString
@Getter
@Setter
@Entity(name = "ReporteJasperSource")
@Table(name = "reporte_jasper_source")
public class ReporteJasperSource implements Serializable {

    @Id
    @Column(name = "reporte_jasper_source_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer reporteJasperSourceId;

    @Column(name = "reporte_jasper_source_dbms")
    private String reporteJasperSourceDbms;

    @Column(name = "reporte_jasper_source_clazz")
    private String reporteJasperSourceClazz;

    @Column(name = "reporte_jasper_source_ip")
    private String reporteJasperSourceIp;

    @Column(name = "reporte_jasper_source_url")
    private String reporteJasperSourceUrl;

    @Column(name = "reporte_jasper_source_db")
    private String reporteJasperSourceDb;

    @Column(name = "reporte_jasper_source_user")
    private String reporteJasperSourceUser;

    @Column(name = "reporte_jasper_source_password")
    private String reporteJasperSourcePassword;
}
