package pe.com.avivel.sistemas.jventas.api.entity;

import java.util.Arrays;
import java.util.List;

public enum NotificacionStatus {

    EXITO("EXITO"),
    ERROR("ERROR"),
    ESPERA("ESPERA");

    private final String status;

    NotificacionStatus(String estado) {
        this.status = estado;
    }

    public static List<NotificacionStatus> getAll() {
        return Arrays.asList(NotificacionStatus.values());
    }

    @Override
    public String toString() {
        return status;
    }
}
