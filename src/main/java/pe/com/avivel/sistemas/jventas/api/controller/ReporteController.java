package pe.com.avivel.sistemas.jventas.api.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.com.avivel.sistemas.jventas.api.controller.dto.ResponseData;
import pe.com.avivel.sistemas.jventas.api.entity.Reporte;
import pe.com.avivel.sistemas.jventas.api.service.exception.ServiceException;
import pe.com.avivel.sistemas.jventas.api.service.inf.ReporteService;

import java.io.Serializable;
import java.util.List;

import static pe.com.avivel.sistemas.jventas.api.util.ConstantsResponse.*;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/reportes")
@RequiredArgsConstructor
public class ReporteController implements Serializable {

    private final ReporteService reporteService;

    @GetMapping
    public ResponseEntity<?> getReportes() {
        List<Reporte> reportes = reporteService.getReportes();
        return ResponseEntity.ok(new ResponseData<>(reportes, SUCCESS, OK, API_VERSION));
    }
    @GetMapping("notificacion/{notificacionId}")
    public ResponseEntity<?> getReportesByNotificacionId(@PathVariable Integer notificacionId) {
        List<Reporte> reportes = reporteService.getReportesByNotificacionId(notificacionId);
        return ResponseEntity.ok(new ResponseData<>(reportes, SUCCESS, OK, API_VERSION));
    }

    @GetMapping("detalle/{reporteId}")
    public ResponseEntity<?> getReporteById(@PathVariable Integer reporteId) {
        try {
            Reporte reporte = reporteService.findById(reporteId);
            return ResponseEntity.ok(new ResponseData<>(reporte, SUCCESS, OK, API_VERSION));
        } catch (ServiceException e) {
            log.error("@@@ Error al obtener Reporte por ID: {}", e.toString());
            return ResponseEntity.ok(new ResponseData<>(null, FAILED, KO, API_VERSION));
        }
    }

    @PostMapping
    public ResponseEntity<?> insertar(@RequestBody Reporte reporte) {
        try {
            return ResponseEntity.ok(new ResponseData<>(reporteService.insert(reporte), SUCCESS, OK, API_VERSION));
        } catch (ServiceException e) {
            log.error("@@@ Error al insertar Notificacion: {}", e.toString());
            return ResponseEntity.ok(new ResponseData<>(null, FAILED, KO, API_VERSION));
        }
    }

    @PutMapping("{id}")
    public ResponseEntity<?> actualizar(@PathVariable Integer id, @RequestBody Reporte reporte) {
        try {
            return ResponseEntity.ok(new ResponseData<>(reporteService.update(reporte, id), SUCCESS, OK, API_VERSION));
        } catch (ServiceException e) {
            log.error("@@@ Error al actulizar Notificacion: {}", e.toString());
            return ResponseEntity.ok(new ResponseData<>(null, FAILED, KO, API_VERSION));
        }
    }
}
