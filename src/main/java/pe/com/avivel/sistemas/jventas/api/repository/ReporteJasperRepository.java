package pe.com.avivel.sistemas.jventas.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.jventas.api.entity.ReporteJasper;

@Repository
public interface ReporteJasperRepository extends JpaRepository<ReporteJasper, Integer> {
}
