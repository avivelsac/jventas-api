package pe.com.avivel.sistemas.jventas.api.job;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pe.com.avivel.sistemas.jventas.api.entity.Notificacion;
import pe.com.avivel.sistemas.jventas.api.entity.NotificacionEstado;
import pe.com.avivel.sistemas.jventas.api.repository.NotificacionRepository;
import pe.com.avivel.sistemas.jventas.api.service.EmailSenderService;
import pe.com.avivel.sistemas.jventas.api.service.ScheduleTaskService;
import pe.com.avivel.sistemas.jventas.api.service.impl.NotificacionServiceImpl;
import pe.com.avivel.sistemas.jventas.api.service.inf.NotificacionService;
import pe.com.avivel.sistemas.jventas.api.util.Fecha;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class TasksJob {

    private final NotificacionService notificacionService;
    private final ScheduleTaskService scheduleTaskService;
    private final EmailSenderService emailSenderService;

    @PostConstruct
    public void init() {
        scheduleTaskService.addTaskToScheduler(1, this::revisarNotificaciones);
    }

    public void revisarNotificaciones() {
        Date fechaHoraActual = new Date();
        log.info("### Fecha/Hora Checking... <- {}", Fecha.toFechaHora(fechaHoraActual));

        List<Notificacion> notificaciones = notificacionService.getNotificacionesByEstado(NotificacionEstado.ACTIVO);

        for (int i = 0; i < notificaciones.size(); i++) {

            Notificacion notificacion = notificaciones.get(i);

            if (notificacion.getNotificacionProximaEjecucion() != null) {

                if (Fecha.compararHastaMinutos(fechaHoraActual, notificacion.getNotificacionProximaEjecucion()) == 0) {
                    log.info("### Notificando {} de {}...", i + 1, notificaciones.size());

                    try {
                        new Thread(() -> emailSenderService.sendNotificacion(notificacion, true)).start();
                    } catch (Exception e) {
                        log.error("@@@ Error al Enviar Notificacion: {} y Fecha/Hora: {}", notificacion.getNotificacionTitulo(), Fecha.toFechaHora(fechaHoraActual));
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
