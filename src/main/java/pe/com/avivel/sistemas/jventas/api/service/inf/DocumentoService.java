package pe.com.avivel.sistemas.jventas.api.service.inf;

import pe.com.avivel.sistemas.jventas.api.entity.Documento;
import pe.com.avivel.sistemas.jventas.api.service.exception.NotFoundException;
import pe.com.avivel.sistemas.jventas.api.service.exception.ServiceException;
import pe.com.avivel.sistemas.jventas.api.service.generic.GenericService;

public interface DocumentoService extends GenericService<Documento> {

    Documento create(Documento documento) throws ServiceException, NotFoundException;
}
