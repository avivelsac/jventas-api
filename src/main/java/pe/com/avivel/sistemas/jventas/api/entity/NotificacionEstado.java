package pe.com.avivel.sistemas.jventas.api.entity;

import java.util.Arrays;
import java.util.List;

public enum NotificacionEstado {

    ACTIVO("ACTIVO"),
    ELIMINADO("ELIMINADO");

    private final String estado;

    NotificacionEstado(String estado) {
        this.estado = estado;
    }

    public static List<NotificacionEstado> getAll() {
        return Arrays.asList(NotificacionEstado.values());
    }

    @Override
    public String toString() {
        return estado;
    }
}
