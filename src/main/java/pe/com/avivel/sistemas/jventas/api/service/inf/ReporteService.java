package pe.com.avivel.sistemas.jventas.api.service.inf;

import pe.com.avivel.sistemas.jventas.api.entity.Reporte;
import pe.com.avivel.sistemas.jventas.api.service.generic.GenericService;

import java.util.List;

public interface ReporteService extends GenericService<Reporte> {

    List<Reporte> getReportes();

    List<Reporte> getReportesByNotificacionId(Integer notificacionId);
}
