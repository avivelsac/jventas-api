package pe.com.avivel.sistemas.jventas.api.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class ResponseData<T> implements Serializable {

    private T data;
    private String message;
    private String status;
    private String apiVersion;

    public ResponseData(T data, String message, String status) {
        this.data = data;
        this.message = message;
        this.status = status;
    }

    public ResponseData(String message, String status) {
        this.message = message;
        this.status = status;
    }

    public ResponseData(T data, String message, String status, String apiVersion) {
        this.data = data;
        this.message = message;
        this.status = status;
        this.apiVersion = apiVersion;
    }

    public ResponseData(String message, String status, String apiVersion) {
        this.message = message;
        this.status = status;
        this.apiVersion = apiVersion;
    }
}
