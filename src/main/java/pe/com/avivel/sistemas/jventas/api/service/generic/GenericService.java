package pe.com.avivel.sistemas.jventas.api.service.generic;

import pe.com.avivel.sistemas.jventas.api.service.exception.ServiceException;

import java.io.Serializable;
import java.util.List;

public interface GenericService<T extends Serializable> {

    T insert(T t) throws ServiceException;

    T update(T t, Integer id) throws ServiceException;

    void delete(Integer id) throws ServiceException;

    T findById(Integer id) throws ServiceException;

    List<T> getAll() throws ServiceException;
}
