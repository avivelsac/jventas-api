package pe.com.avivel.sistemas.jventas.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.jventas.api.entity.Articulo;

@Repository
public interface ArticuloRepository extends JpaRepository<Articulo, Integer> {

    @Query("select a from Articulo a where a.codigo=:codigo")
    Articulo findByCodigo(@Param("codigo") String codigo);
}
