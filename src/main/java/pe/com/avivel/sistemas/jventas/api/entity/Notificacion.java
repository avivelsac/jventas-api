package pe.com.avivel.sistemas.jventas.api.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Getter
@Setter
@Entity(name = "Notificacion")
@Table(name = "notificacion")
public class Notificacion implements Serializable {

    @Id
    @Column(name = "notificacion_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer notificacionId;

    @Column(name = "notificacion_titulo")
    private String notificacionTitulo;

    @Column(name = "notificacion_destinatarios")
    private String notificacionDestinatarios;

    @Column(name = "notificacion_mensaje")
    private String notificacionMensaje;

    @Column(name = "notificacion_cantidad_enviar")
    private Integer notificacionCantidadEnviar;

    @Column(name = "notificacion_cantidad_enviados")
    private Integer notificacionCantidadEnviados;

    @Column(name = "notificacion_intervalo")
    @Enumerated(EnumType.STRING)
    private NotificacionIntervalo notificacionIntervalo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "notificacion_primera_ejecucion")
    private Date notificacionPrimeraEjecucion;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "notificacion_ultima_ejecucion")
    private Date notificacionUltimaEjecucion;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "notificacion_proxima_ejecucion")
    private Date notificacionProximaEjecucion;

    @Column(name = "notificacion_status")
    @Enumerated(EnumType.STRING)
    private NotificacionStatus notificacionStatus;

    @Column(name = "notificacion_estado")
    @Enumerated(EnumType.STRING)
    private NotificacionEstado notificacionEstado;

    @Transient
    private List<Reporte> reportes = new ArrayList<>();

    public Notificacion setNotificacionId(Integer notificacionId) {
        this.notificacionId = notificacionId;
        return this;
    }

}
