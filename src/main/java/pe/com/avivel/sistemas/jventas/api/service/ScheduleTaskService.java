package pe.com.avivel.sistemas.jventas.api.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;
import pe.com.avivel.sistemas.jventas.api.properties.JVentasProperties;

import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ScheduledFuture;

@Slf4j
@Service
@RequiredArgsConstructor
public class ScheduleTaskService {

    private final TaskScheduler scheduler;
    private final JVentasProperties properties;
    private final Map<Integer, ScheduledFuture<?>> jobsMap = new HashMap<>();

    public void addTaskToScheduler(int id, Runnable task) {
        ScheduledFuture<?> scheduledTask = scheduler.schedule(task, new CronTrigger(properties.getJob().getFrecuencia(), TimeZone.getTimeZone(TimeZone.getDefault().getID())));
        jobsMap.put(id, scheduledTask);
    }

    public void removeTaskFromScheduler(int id) {
        ScheduledFuture<?> scheduledTask = jobsMap.get(id);
        if (scheduledTask != null) {
            scheduledTask.cancel(true);
            jobsMap.put(id, null);
        }
    }

    @EventListener({ContextRefreshedEvent.class})
    public void contextRefreshedEvent() {
        log.info("### contextRefreshedEvent... ###");
    }
}
