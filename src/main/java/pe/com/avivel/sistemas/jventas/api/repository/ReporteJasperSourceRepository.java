package pe.com.avivel.sistemas.jventas.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.jventas.api.entity.ReporteJasperSource;

@Repository
public interface ReporteJasperSourceRepository extends JpaRepository<ReporteJasperSource, Integer> {
}
