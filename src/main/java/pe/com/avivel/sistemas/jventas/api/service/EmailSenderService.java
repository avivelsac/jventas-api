package pe.com.avivel.sistemas.jventas.api.service;

import bsh.Interpreter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.StringSubstitutor;
import org.springframework.stereotype.Service;
import pe.com.avivel.sistemas.jventas.api.entity.*;
import pe.com.avivel.sistemas.jventas.api.properties.JVentasProperties;
import pe.com.avivel.sistemas.jventas.api.repository.NotificacionRepository;
import pe.com.avivel.sistemas.jventas.api.util.ConvertUtil;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.beans.Statement;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailSenderService {

    private final DataSource dataSource;
    private final JVentasProperties properties;
    private final EmailService emailService;
    private final JasperReportService jasperReportService;
    private final NotificacionRepository notificacionRepository;

    @Transactional
    public boolean sendNotificacion(Notificacion notificacion, boolean actualizarProximaEjecucion) {
        boolean enviado = false;

        if (notificacion != null) {
            log.info("@@@ Enviado Notificacion {}, con Titulo {}...", notificacion.getNotificacionId(), notificacion.getNotificacionTitulo());

            List<String> reportesEnviados = new ArrayList<>();
            // Generar PDFs y guardarlos en Carpeta
            boolean next = generatePDFsByNotificacion(notificacion);

            // Emviar Correo con los PDFs de Reportes adjuntos desde su Carpeta
            if (next) {

                reportesEnviados = getPathsAttachtments(notificacion.getReportes());

                next = emailService.send(
                        properties.getMail().getAlias(),
                        properties.getMail().getCorreo(),
                        notificacion.getNotificacionDestinatarios().split(","),
                        "JVentas: " + notificacion.getNotificacionTitulo(),
                        notificacion.getNotificacionMensaje(),
                        reportesEnviados);
            }

            // Actualiza las Cantidades a Enviar y Enviados
            if (next) {
                next = notificacionRepository.updateCantidades(
                        notificacion.getReportes().size(),
                        reportesEnviados.size(),
                        notificacion.getNotificacionId()) > 0;
            }

            // Actualiza las Fechas de Ejecucion
            if (next) {
                if (actualizarProximaEjecucion) {
                    Date nuevaProximaEjecucion = null;

                    if (notificacion.getNotificacionIntervalo().equals(NotificacionIntervalo.DIA))
                        nuevaProximaEjecucion = ConvertUtil.addDaysFull(notificacion.getNotificacionProximaEjecucion(), 1);

                    if (notificacion.getNotificacionIntervalo().equals(NotificacionIntervalo.SEMANA))
                        nuevaProximaEjecucion = ConvertUtil.addDaysFull(notificacion.getNotificacionProximaEjecucion(), 7);

                    next = notificacionRepository.updateFechasEjecuciones(
                            new Date(),
                            nuevaProximaEjecucion,
                            notificacion.getNotificacionId()) > 0;
                } else {

                    next = notificacionRepository.updateUltimaEjecucion(
                            new Date(),
                            notificacion.getNotificacionId()) > 0;
                }
            }

            // Actualiza el Status de la Notificacion
            if (next) {
                notificacionRepository.upateStatusNotificacion(NotificacionStatus.EXITO, notificacion.getNotificacionId());
                log.info("@@@ Notificacion finalizada completamente con exito.");
            } else {
                notificacionRepository.upateStatusNotificacion(NotificacionStatus.ERROR, notificacion.getNotificacionId());
                log.info("@@@ Notificacion finalizada con errores.");
            }

            enviado = next;
        }

        return enviado;
    }

    private boolean generatePDFsByNotificacion(Notificacion notificacion) {
        AtomicBoolean status = new AtomicBoolean(false);

        notificacion.getReportes().forEach(r -> {

            if (r.getReporteJasper() != null && r.getReporteEstado() == ReporteEstado.HABILITADO) {

                try {
                    Map<String, Object> parametros = new HashMap<>();
                    for (ReporteParametro p : r.getParametros()) {

                        Interpreter interpreter = new Interpreter();
                        interpreter.eval("import pe.com.avivel.sistemas.jventas.api.util.Fecha");

                        if (p.isReporteParametroUseOthers()) {
                            String newValue = replaceVariables(r.getParametros(), p);
                            parametros.put(p.getReporteParametroKey(), interpreter.eval(newValue));
                        } else {
                            parametros.put(p.getReporteParametroKey(), interpreter.eval(p.getReporteParametroValue()));
                        }
                    }

                    new Statement(this,
                            "generateAndSaveReport",
                            new Object[]{parametros, r.getReporteJasper()}
                    ).execute();

                    status.set(true);
                } catch (Exception e) {
                    status.set(false);
                    e.printStackTrace();
                }
            }
        });

        return status.get();
    }

    private String replaceVariables(List<ReporteParametro> reporteParametros, ReporteParametro paramToUpdate) {
        Pattern regex = Pattern.compile("\\{(.*?)}");
        List<String> keysToUpdate = new ArrayList<>();

        String str = paramToUpdate.getReporteParametroValue();
        log.info("··· oldParam: {}", str);

        Matcher regexMatcher = regex.matcher(str);
        while (regexMatcher.find()) {
            keysToUpdate.add(regexMatcher.group(1));
        }
        log.info("··· keysToUpdate: {}", keysToUpdate);

        Map<String, Object> tempParams = new HashMap<>();
        keysToUpdate.forEach(
                k -> reporteParametros.forEach(
                        p -> {
                            if (k.equals(p.getReporteParametroKey())) {
                                tempParams.put(k, p.getReporteParametroValue());
                            }
                        }
                )
        );

        log.info("··· tempParams: {}", tempParams);
        log.info("··· newParam: {}", StringSubstitutor.replace(str, tempParams));
        return StringSubstitutor.replace(str, tempParams);
    }

    @SuppressWarnings("unused")
    public void generateAndSaveReport(Map<String, Object> parametros, ReporteJasper reporteJasper) throws Exception {
        log.info("@@@ Conectando a Base de Datos: {}", reporteJasper.getReporteJasperSource());
        try (Connection con = getDynamicConnection(reporteJasper.getReporteJasperSource())) {
            byte[] bytes = jasperReportService.pdfToByteArray(con, reporteJasper.getReporteJasperFile(), parametros);
            File file = new File(properties.getMail().getReportesPath() + reporteJasper.getReporteJasperOut());
            OutputStream os = new FileOutputStream(file);
            os.write(bytes);
            os.close();
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    private Connection getDynamicConnection(ReporteJasperSource source) throws SQLException, ClassNotFoundException {
        Class.forName(source.getReporteJasperSourceClazz());
        return DriverManager.getConnection(
                source.getReporteJasperSourceUrl() + "/" + source.getReporteJasperSourceDb(),
                source.getReporteJasperSourceUser(),
                source.getReporteJasperSourcePassword()
        );
    }

    private List<String> getPathsAttachtments(List<Reporte> reportes) {
        List<String> paths = new ArrayList<>();
        reportes.forEach(r -> {
            if (r.getReporteJasper().getReporteJasperOut() != null)
                paths.add(properties.getMail().getReportesPath() + r.getReporteJasper().getReporteJasperOut());
        });
        return paths;
    }
}
