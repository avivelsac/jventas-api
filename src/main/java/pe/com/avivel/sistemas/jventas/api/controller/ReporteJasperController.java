package pe.com.avivel.sistemas.jventas.api.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.com.avivel.sistemas.jventas.api.controller.dto.ResponseData;
import pe.com.avivel.sistemas.jventas.api.entity.ReporteJasper;
import pe.com.avivel.sistemas.jventas.api.service.exception.ServiceException;
import pe.com.avivel.sistemas.jventas.api.service.inf.ReporteJasperService;

import java.io.Serializable;
import java.util.List;

import static pe.com.avivel.sistemas.jventas.api.util.ConstantsResponse.*;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/reporte-jaspers")
@RequiredArgsConstructor
public class ReporteJasperController implements Serializable {

    private final ReporteJasperService reporteJasperService;

    @GetMapping
    public ResponseEntity<?> getReporteJaspers() {
        try {
            List<ReporteJasper> reporteJaspers = reporteJasperService.getAll();
            return ResponseEntity.ok(new ResponseData<>(reporteJaspers, SUCCESS, OK, API_VERSION));
        } catch (ServiceException e) {
            log.info("@@@ Error al obtener jaspers: {}", e.toString());
            return ResponseEntity.ok(new ResponseData<>(null, SUCCESS, OK, API_VERSION));
        }
    }

    @GetMapping("detalle/{reporteJasperId}")
    public ResponseEntity<?> getNotificacionById(@PathVariable Integer reporteJasperId) {
        try {
            ReporteJasper reporteJasper = reporteJasperService.findById(reporteJasperId);
            return ResponseEntity.ok(new ResponseData<>(reporteJasper, SUCCESS, OK, API_VERSION));
        } catch (ServiceException e) {
            log.error("@@@ Error al obtener Notificacion por ID: {}", e.toString());
            return ResponseEntity.ok(new ResponseData<>(null, FAILED, KO, API_VERSION));
        }
    }
}
