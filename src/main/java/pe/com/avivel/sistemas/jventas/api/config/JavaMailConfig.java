package pe.com.avivel.sistemas.jventas.api.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import pe.com.avivel.sistemas.jventas.api.properties.JVentasProperties;

import java.util.Properties;

@Configuration
@RequiredArgsConstructor
public class JavaMailConfig {

    private final JVentasProperties properties;

    @Bean
    public JavaMailSender mailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setUsername(properties.getMail().getCorreo());
        mailSender.setPassword(properties.getMail().getContrasena());
        mailSender.setHost(properties.getMail().getHost());
        mailSender.setPort(Integer.parseInt(properties.getMail().getPuerto()));

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.smtp.auth", properties.getMail().isAuth());
        props.put("mail.smtp.starttls.enable", properties.getMail().isStarttls());
        props.put("mail.transport.protocol", properties.getMail().getProtocolo());

        mailSender.setJavaMailProperties(props);

        return mailSender;
    }
}