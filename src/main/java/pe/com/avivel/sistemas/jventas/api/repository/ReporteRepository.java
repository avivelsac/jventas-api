package pe.com.avivel.sistemas.jventas.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.jventas.api.entity.Reporte;

import java.util.List;

@Repository
public interface ReporteRepository extends JpaRepository<Reporte, Integer> {

    @Query(value = " select r.* " +
            " from reporte r " +
            " join notificacion_reporte nr on r.reporte_id  = nr.reporte_id" +
            " where nr.notificacion_id=:notificacionId ", nativeQuery = true)
    List<Reporte> findAllByNotificacionId(Integer notificacionId);
}
