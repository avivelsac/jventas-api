package pe.com.avivel.sistemas.jventas.api.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity(name = "Articulo")
@Table(name = "articulo")
@Access(AccessType.FIELD)
public class Articulo implements Serializable {

    @Id
    @Column(name = "articulo_id")
    private Integer id;

    @Column(name = "articulo_codigo")
    private String codigo;

    @Column(name = "articulo_descripcion")
    private String descripcion;
}
