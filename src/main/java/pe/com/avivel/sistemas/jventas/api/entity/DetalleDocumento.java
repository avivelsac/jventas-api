package pe.com.avivel.sistemas.jventas.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Entity(name = "DetalleDocumento")
@Table(name = "detalle_documento")
@Access(AccessType.FIELD)
public class DetalleDocumento implements Serializable {

    @Id
    @Column(name = "detalle_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "detalle_secuencia")
    private Integer secuencia;

    @JoinColumn(name = "documento_id", referencedColumnName = "documento_id")
    @ManyToOne(optional = false, targetEntity = Documento.class)
    @JsonIgnore
    private Documento documento;

    @JoinColumn(name = "articulo_id", referencedColumnName = "articulo_id")
    @ManyToOne(optional = false, targetEntity = Articulo.class)
    private Articulo articulo;

    @Column(name = "detalle_unidades")
    private BigDecimal unidades;

    @Column(name = "detalle_cantidad")
    private BigDecimal cantidad;

    @Column(name = "detalle_precio")
    private BigDecimal precio;

    @Column(name = "detalle_importe")
    private BigDecimal importe;

    @Column(name = "detalle_tipoafectacionigv")
    @Enumerated(EnumType.STRING)
    private TipoAfectacionIGV tipoAfectacionIGV;
}
