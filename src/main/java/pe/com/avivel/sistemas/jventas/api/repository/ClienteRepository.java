package pe.com.avivel.sistemas.jventas.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.jventas.api.entity.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {

    Cliente findClienteByNumeroDocumentoIdentidad(String numeroDocumentoIdentidad);
}
