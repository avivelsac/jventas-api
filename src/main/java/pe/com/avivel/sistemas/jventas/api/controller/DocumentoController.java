package pe.com.avivel.sistemas.jventas.api.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.com.avivel.sistemas.jventas.api.entity.Documento;
import pe.com.avivel.sistemas.jventas.api.service.exception.NotFoundException;
import pe.com.avivel.sistemas.jventas.api.service.exception.ServiceException;
import pe.com.avivel.sistemas.jventas.api.service.inf.DocumentoService;

import java.io.Serializable;

@Slf4j
@RestController
@RequestMapping("api/documentos")
public class DocumentoController implements Serializable {

    private final DocumentoService documentoService;

    @Autowired
    public DocumentoController(DocumentoService documentoService) {
        this.documentoService = documentoService;
    }

    @PostMapping
    public ResponseEntity<Documento> create(@RequestBody String request) {
        try {
            ObjectMapper om = new ObjectMapper();
            om.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);

            Documento d = om.readValue(request, Documento.class);

            log.info("### JVENTAS-API: PREPARED: {}", d);

            Documento documentoSaved = documentoService.create(d);

            log.info("### JVENTAS-API: SAVED: {}", documentoSaved);

            return ResponseEntity.ok(documentoSaved);
        } catch (ServiceException e) {

            log.error("### JVENTAS-API: ServiceException");
            log.error("### DocumentoController> create: ", e);

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (NotFoundException | JsonProcessingException e) {

            log.error("### JVENTAS-API: NotFoundException | JsonProcessingException");
            log.error("### DocumentoController> create: ", e);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Documento> findById(@PathVariable("id") Integer id) throws ServiceException {
        try {

            Documento documento = documentoService.findById(id);

            log.info("### JVENTAS-API: GETBYID: {}", documento);

            if (documento == null) {
                return ResponseEntity.notFound().build();
            }

            return ResponseEntity.ok(documento);
        } catch (ServiceException e) {

            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
