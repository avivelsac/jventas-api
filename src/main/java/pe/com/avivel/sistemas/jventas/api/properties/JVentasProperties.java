package pe.com.avivel.sistemas.jventas.api.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(value = "jventas")
@PropertySource("classpath:/application.properties")
public class JVentasProperties {

    private String ruc;
    private String razonSocial;
    private MailProperties mail;
    private JobProperties job;

    @Getter
    @Setter
    public static class MailProperties {

        private String correo;
        private String contrasena;
        private String alias;
        private String host;
        private String protocolo;
        private String puerto;
        private boolean auth;
        private boolean starttls;
        private String reportesPath;
        private PlantillasProperties plantillas;
    }

    @Getter
    @Setter
    public static class PlantillasProperties {

        private String correoDefault;

    }

    @Getter
    @Setter
    public static class JobProperties {

        private String frecuencia;
    }
}
