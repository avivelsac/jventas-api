package pe.com.avivel.sistemas.jventas.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.jventas.api.entity.Documento;

@Repository
public interface DocumentoRepository extends JpaRepository<Documento, Integer> {

    @Query(" select d " +
            "from Documento d " +
            "where d.tipoDocumento.id=:tipoDocumentoId " +
            " and d.serie=:serie" +
            " and d.numero=:numero ")
    Documento getDocumentoByTipoAndSerieAndNumero(@Param("tipoDocumentoId") Integer tipoDocumentoId,
                                                  @Param("serie") String serie,
                                                  @Param("numero") String numero);
}
