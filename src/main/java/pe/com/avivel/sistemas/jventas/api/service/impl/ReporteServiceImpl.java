package pe.com.avivel.sistemas.jventas.api.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import pe.com.avivel.sistemas.jventas.api.entity.Reporte;
import pe.com.avivel.sistemas.jventas.api.repository.ReporteRepository;
import pe.com.avivel.sistemas.jventas.api.service.exception.ServiceException;
import pe.com.avivel.sistemas.jventas.api.service.inf.ReporteService;
import pe.com.avivel.sistemas.jventas.api.util.Texto;

import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class ReporteServiceImpl implements ReporteService {

    private final ReporteRepository reporteRepository;

    @Override
    public Reporte insert(Reporte reporte) throws ServiceException {
        Reporte inserted = reporteRepository.save(reporte);
        inserted.getParametros().forEach(param -> {
            param.setReporte(inserted);
        });
        return reporteRepository.save(inserted);
    }

    @Override
    public Reporte update(Reporte reporte, Integer id) throws ServiceException {
        Reporte fetched = reporteRepository.getById(id);
        BeanUtils.copyProperties(reporte, fetched, Texto.ignoreNullsFrom(reporte));
        fetched.getParametros().forEach(param -> {
            param.setReporte(reporte);
        });
        return reporteRepository.save(fetched);
    }

    @Override
    public void delete(Integer id) throws ServiceException {
    }

    @Override
    public Reporte findById(Integer id) throws ServiceException {
        return reporteRepository.findById(id).orElse(null);
    }

    @Override
    public List<Reporte> getAll() throws ServiceException {
        return reporteRepository.findAll();
    }

    @Override
    public List<Reporte> getReportes() {
        return reporteRepository.findAll();
    }

    @Override
    public List<Reporte> getReportesByNotificacionId(Integer notificacionId) {
        return reporteRepository.findAllByNotificacionId(notificacionId);
    }
}
