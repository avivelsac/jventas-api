package pe.com.avivel.sistemas.jventas.api.util;

import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class ConvertUtil {

    private final static String INFINITY_FUTURE_DATE = "31/12/2999";
    private final static String INFINITY_PAST_DATE = "01/01/1900";
    private final static String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
    private final static String DEFAULT_DATE_FORMAT_DASHES = "dd-MM-yyyy";
    private final static String DEFAULT_DATETIME_FORMAT = "dd/MM/yyyy HH:mm:ss.SSS";
    private final static String DEFAULT_DATETIME_FORMAT_WMS = "dd/MM/yyyy HH:mm:ss";
    private final static String DEFAULT_DATETIME_FORMAT_WS = "ddMMyyyyHHmmss";
    private final static String ISO_DATE_FORMAT = "yyyy-MM-dd";

    public static String dateToIsoDateFormat(Date date) {
        if (date == null) return null;
        return new SimpleDateFormat(ISO_DATE_FORMAT).format(date);
    }

    public static String toDate(Date date) {
        if (date == null) return null;
        return new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(date);
    }

    public static String toDateWithDashes(Date date) {
        if (date == null) return null;
        return new SimpleDateFormat(DEFAULT_DATE_FORMAT_DASHES).format(date);
    }

    public static String toDateTime(Date date) {
        if (date == null) return null;
        return new SimpleDateFormat(DEFAULT_DATETIME_FORMAT_WMS).format(date);
    }

    public static Date toDate(Object date) {
        if (date == null) return null;
        return (Date) date;
    }

    public static String geDefaultDateTime() {
        return new SimpleDateFormat(DEFAULT_DATETIME_FORMAT).format(new Date());
    }

    public static String getDateTimeNoSpaces() {
        return new SimpleDateFormat(DEFAULT_DATETIME_FORMAT_WS).format(new Date());
    }

    public static Date toDate(Long value) {
        if (value == null) {
            return null;
        } else {
            Date date = new Date(value);
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            try {
                return formatter.parse(formatter.format(date));
            } catch (ParseException e) {
                return null;
            }
        }
    }

    public static Date toDate(String value) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return formatter.parse(value);
        } catch (ParseException e) {
            return null;
        }
    }

    public static Date toDateTime(Long value) {
        if (value == null) {
            return null;
        } else {
            Date date = new Date(value);
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
            try {
                return formatter.parse(formatter.format(date));
            } catch (ParseException e) {
                return null;
            }
        }
    }

    public static Boolean toBoolean(String val) {
        if (StringUtils.isEmpty(val))
            return null;
        else
            return Boolean.valueOf(val);
    }

    public static String getDefaultDate(Date date) {
        return new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(date);
    }

    public static String nowDateFormat(String format) {
        try {
            return new SimpleDateFormat(format).format(new Date());
        } catch (Exception e) {
            return UUID.randomUUID().toString().replace("-", "");
        }
    }

    public static BigDecimal getBigDecimal(Object value) {
        BigDecimal ret = null;
        if (value != null) {
            if (value instanceof BigDecimal) {
                ret = (BigDecimal) value;
            } else if (value instanceof String) {
                ret = new BigDecimal((String) value);
            } else if (value instanceof BigInteger) {
                ret = new BigDecimal((BigInteger) value);
            } else if (value instanceof Number) {
                ret = new BigDecimal(((Number) value).doubleValue());
            } else {
                throw new ClassCastException("Not possible to coerce [" + value + "] from class " + value.getClass() + " into a BigDecimal.");
            }
        }
        return ret;
    }

    public static BigInteger getBigInteger(Object value) {
        BigInteger ret = null;
        if (value != null) {
            if (value instanceof BigInteger) {
                ret = (BigInteger) value;
            } else if (value instanceof String) {
                ret = new BigInteger((String) value);
            } else {
                throw new ClassCastException("Not possible to coerce [" + value + "] from class " + value.getClass() + " into a BigInteger.");
            }
        }
        return ret;
    }

    public static Date addDays(Date date, int dias) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DAY_OF_YEAR, dias);
        return calendar.getTime();
    }

    public static Date addDaysFull(Date date, int dias) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, dias);
        return calendar.getTime();
    }

    public static Date addHours(Date date, int horas) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY, horas);
        return calendar.getTime();
    }

    public static Date toDateDDMMYYYY(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
}
