package pe.com.avivel.sistemas.jventas.api.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailService {

    private final JavaMailSender mailSender;

    public boolean send(String alias, String from, String[] emails, String subject, String body, List<String> paths) {
        boolean status;
        try {
            log.info("Sending Email to {}", Arrays.toString(emails));
            MimeMessage message = mailSender.createMimeMessage();

            MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, StandardCharsets.UTF_8.toString());
            messageHelper.setFrom(from, alias);
            messageHelper.setTo(emails);
            messageHelper.setSubject(subject);
            messageHelper.setText(body, true);

            for (String path : paths) {
                FileSystemResource file = new FileSystemResource(path);
                messageHelper.addAttachment(Objects.requireNonNull(file.getFilename()), file);
            }

            mailSender.send(message);

            status = true;
            log.info("@@@ Correo '{}' enviado a los correos {} correctamente", subject, Arrays.toString(emails));
        } catch (MessagingException | UnsupportedEncodingException e) {
            log.error("Failed to send email to {}", Arrays.toString(emails), e);
            status = false;
        }

        return status;
    }

    public void sendMail(String alias, String fromEmail, String toEmail, String subject, String body) {
        try {
            log.info("Sending Email to {}", toEmail);
            MimeMessage message = mailSender.createMimeMessage();

            MimeMessageHelper messageHelper = new MimeMessageHelper(message, StandardCharsets.UTF_8.toString());
            messageHelper.setSubject(subject);
            messageHelper.setText(body, true);
            messageHelper.setFrom(fromEmail, alias);
            messageHelper.setTo(toEmail);

            mailSender.send(message);
        } catch (MessagingException | UnsupportedEncodingException e) {
            log.error("Failed to send email to {}", toEmail, e);
        }
    }
}
