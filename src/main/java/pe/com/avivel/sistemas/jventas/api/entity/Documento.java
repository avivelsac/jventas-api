package pe.com.avivel.sistemas.jventas.api.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;
import pe.com.avivel.sistemas.jventas.api.util.ConvertUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@Entity(name = "Documento")
@Table(name = "documento")
@JsonRootName("documento")
@Access(AccessType.FIELD)
public class Documento implements Serializable {

    @Id
    @Column(name = "documento_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "documento_baseimponible")
    private BigDecimal baseImponible;

    @JoinColumn(name = "canalventa_id", referencedColumnName = "canalventa_id")
    @ManyToOne(optional = false, targetEntity = CanalVenta.class)
    private CanalVenta canalVenta;

    @JoinColumn(name = "cliente_id_1", referencedColumnName = "cliente_id")
    @ManyToOne(targetEntity = Cliente.class)
    private Cliente cliente;

    @JoinColumn(name = "cliente_id_2", referencedColumnName = "cliente_id")
    @ManyToOne(targetEntity = Cliente.class)
    private Cliente clienteDestino;

    @Column(name = "documento_cuenta")
    private String cuenta;

    @Column(name = "documento_digestvalue")
    private String digestValue;

    @Temporal(TemporalType.DATE)
    @Column(name = "documento_fechaemision")
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date fechaEmision;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "documento_fechaenvioxml")
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date fechaEnvioXml;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "documento_fechageneracionxml")
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date fechaGeneracionXml;

    @Temporal(TemporalType.DATE)
    @Column(name = "documento_fechavencimiento")
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date fechaVencimiento;

    @Column(name = "documento_glosa")
    private String glosa;

    @Column(name = "documento_glosa2")
    private String glosa2;

    @Column(name = "documento_igv")
    private BigDecimal igv;

    @Column(name = "documento_importe")
    private BigDecimal importe;

    @Column(name = "documento_impreso")
    private Boolean impreso;

    @Column(name = "documento_itinerante")
    private Boolean itinerante;

    @JoinColumn(name = "mediopago_id", referencedColumnName = "mediopago_id")
    @ManyToOne(optional = false, targetEntity = MedioPago.class)
    private MedioPago medioPago;

    @Column(name = "documento_moneda")
    @Enumerated(EnumType.STRING)
    private Moneda moneda;

    @Column(name = "documento_numero")
    private String numero;

    @Column(name = "documento_observaciones")
    private String observaciones;

    @Column(name = "documento_opexoneradas")
    private BigDecimal operacionesExoneradas;

    @Column(name = "documento_opgratuitas")
    private BigDecimal operacionesGratuitas;

    @Column(name = "documento_opinafectas")
    private BigDecimal operacionesInafectas;

    @Column(name = "documento_porcentajeigv")
    private BigDecimal porcentajeIgv;

    @Column(name = "documento_saldo")
    private BigDecimal saldo;

    @Column(name = "documento_semana")
    private Integer semana;

    @Column(name = "documento_semana2")
    private Integer semana2;

    @Column(name = "documento_serie")
    private String serie;

    @Column(name = "documento_situacion")
    private String situacion;

    @Column(name = "documento_subdiario")
    @Enumerated(EnumType.STRING)
    private Subdiario subdiario;

    @Column(name = "documento_sujetospot")
    private Boolean sujetoSpot;

    @Column(name = "documento_ticket")
    private String ticket;

    @Column(name = "documento_tipocambio")
    private BigDecimal tipoCambio;

    @Column(name = "documento_tipoconversion")
    @Enumerated(EnumType.STRING)
    private TipoConversion tipoConversion;

    @JoinColumn(name = "tipodocumento_id", referencedColumnName = "tipodocumento_id")
    @ManyToOne(optional = false, targetEntity = TipoDocumento.class)
    private TipoDocumento tipoDocumento;

    @Column(name = "documento_tipooperacion")
    @Enumerated(EnumType.STRING)
    private TipoOperacion tipoOperacion;

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "documento",
            orphanRemoval = true,
            targetEntity = DetalleDocumento.class,
            fetch = FetchType.EAGER)
    private List<DetalleDocumento> detalleDocumentoList;

    @Override
    public String toString() {
        return String.valueOf(tipoDocumento.getId())
                .concat(" / ")
                .concat(serie)
                .concat("-")
                .concat(numero)
                .concat(" / ")
                .concat(ConvertUtil.toDate(fechaEmision));
    }
}
