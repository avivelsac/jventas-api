package pe.com.avivel.sistemas.jventas.api.entity;

public enum ReporteEstado {

    HABILITADO("HABILITADO"),
    DESHABILITADO("DESHABILITADO");

    private final String estado;

    ReporteEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return estado;
    }
}
