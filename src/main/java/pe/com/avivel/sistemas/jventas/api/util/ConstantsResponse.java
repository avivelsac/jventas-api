package pe.com.avivel.sistemas.jventas.api.util;

public class ConstantsResponse {

    public static final String API_VERSION = "1.1.0";
    public static final String NO_DATA = "No data";
    public static final String EXIST_EMAIL = "Exist Email";
    public static final String OK = "OK";
    public static final String KO = "KO";
    public static final String SUCCESS = "SUCCESS";
    public static final String FAILED = "FAILED";
}
