package pe.com.avivel.sistemas.jventas.api.util;

import java.util.Date;

public class RptCodigosConstants {

    public final static String KGR = "KGR";
    public final static String OTROS = "OTR";
    public final static String GALLINAS = "GAL";
    public final static String HUEVOS = "HUE";
    public final static Date diaAnterior = ConvertUtil.addDays(new Date(), -1);

    public final static String ART_CLI_KGR_HUEVO_1 = "ART-CLI-KGR-HUEVO-1";
    public final static String ART_CLI_KGR_GALLINA_1 = "ART-CLI-KGR-GALLINA-1";
    public final static String ART_CLI_KGR_ABONO_1 = "ART-CLI-KGR-ABONO-1";
    public final static String ART_CLI_KGR_OTROS_1 = "ART-CLI-KGR-OTROS-1";
    public final static String ART_CLI_OTROS_HUEVO_1 = "ART-CLI-OTROS-HUEVO-1";
}
