package pe.com.avivel.sistemas.jventas.api.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.type.PdfVersionEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class JasperReportService {

    private final ResourceLoader resourceLoader;

    public byte[] pdfToByteArray(Connection connection, String reporte, Map<String, Object> parameters) throws Exception {
        try {
            String path = resourceLoader.getResource("classpath:reports/" + reporte).getURI().getPath();

            if (parameters == null) parameters = new HashMap<>();

            JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(path);
            JasperReportsContext jasperReportsContext = DefaultJasperReportsContext.getInstance();

            JRPropertiesUtil jrPropertiesUtil = JRPropertiesUtil.getInstance(jasperReportsContext);
            jrPropertiesUtil.setProperty("net.sf.jasperreports.query.executer.factory.plsql", "net.sf.jasperreports.engine.query.PlSqlQueryExecuterFactory");

            JasperPrint jasperPrint = null;

            SimplePdfExporterConfiguration pdfExporterConfiguration = new SimplePdfExporterConfiguration();
            pdfExporterConfiguration.setCompressed(true);
            pdfExporterConfiguration.setPdfVersion(PdfVersionEnum.VERSION_1_5);

            if (connection != null && !connection.isClosed()) {
                jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection);
            }

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            JRPdfExporter pdfExporter = new JRPdfExporter();
            pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
            pdfExporter.setConfiguration(pdfExporterConfiguration);
            pdfExporter.exportReport();

            byte[] rawBytes = outputStream.toByteArray();

            outputStream.write(0);
            outputStream.flush();
            outputStream.close();
            pdfExporter.reset();

            return rawBytes;
        } catch (Exception e) {
            log.error("### Error en exportReportToPdf: {0}", e);
            throw new Exception(e);
        }
    }
}
