package pe.com.avivel.sistemas.jventas.api.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.com.avivel.sistemas.jventas.api.controller.dto.ResponseData;
import pe.com.avivel.sistemas.jventas.api.entity.Notificacion;
import pe.com.avivel.sistemas.jventas.api.entity.NotificacionEstado;
import pe.com.avivel.sistemas.jventas.api.service.exception.ServiceException;
import pe.com.avivel.sistemas.jventas.api.service.inf.NotificacionService;

import java.io.Serializable;
import java.util.List;

import static pe.com.avivel.sistemas.jventas.api.util.ConstantsResponse.*;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/notificaciones")
@RequiredArgsConstructor
public class NotificacionController implements Serializable {

    private final NotificacionService notificacionService;

    @GetMapping
    public ResponseEntity<?> getNotificacionesActivas() {
        List<Notificacion> notificaciones = notificacionService.getNotificacionesByEstado(NotificacionEstado.ACTIVO);
        return ResponseEntity.ok(new ResponseData<>(notificaciones, SUCCESS, OK, API_VERSION));
    }

    @GetMapping("detalle/{notificacionId}")
    public ResponseEntity<?> getNotificacionById(@PathVariable Integer notificacionId) {
        try {
            Notificacion notificacion = notificacionService.findById(notificacionId);
            return ResponseEntity.ok(new ResponseData<>(notificacion, SUCCESS, OK, API_VERSION));
        } catch (ServiceException e) {
            log.error("@@@ Error al obtener Notificacion por ID: {}", e.toString());
            return ResponseEntity.ok(new ResponseData<>(null, FAILED, KO, API_VERSION));
        }
    }

    @PostMapping
    public ResponseEntity<?> insertar(@RequestBody Notificacion notificacion) {
        try {
            return ResponseEntity.ok(new ResponseData<>(notificacionService.insert(notificacion), SUCCESS, OK, API_VERSION));
        } catch (ServiceException e) {
            log.error("@@@ Error al insertar Notificacion: {}", e.toString());
            return ResponseEntity.ok(new ResponseData<>(null, FAILED, KO, API_VERSION));
        }
    }

    @PutMapping("{id}")
    public ResponseEntity<?> actualizar(@PathVariable Integer id, @RequestBody Notificacion notificacion) {
        try {
            return ResponseEntity.ok(new ResponseData<>(notificacionService.update(notificacion, id), SUCCESS, OK, API_VERSION));
        } catch (ServiceException e) {
            log.error("@@@ Error al actulizar Notificacion: {}", e.toString());
            return ResponseEntity.ok(new ResponseData<>(null, FAILED, KO, API_VERSION));
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> eliminar(@PathVariable Integer id) {
        try {
            notificacionService.delete(id);
            return ResponseEntity.ok(new ResponseData<>(id, SUCCESS, OK, API_VERSION));
        } catch (ServiceException e) {
            log.error("@@@ Error al eliminar Notificacion: {}", e.toString());
            return ResponseEntity.ok(new ResponseData<>(null, FAILED, KO, API_VERSION));
        }
    }

    @PostMapping("notificacion-reporte/{notificacionId}/{reporteId}")
    public ResponseEntity<?> insertarNotificacionReporte(@PathVariable Integer notificacionId,
                                                         @PathVariable Integer reporteId) {
        try {
            int val = notificacionService.saveNotificacionReporte(notificacionId, reporteId);
            return ResponseEntity.ok(new ResponseData<>(val, SUCCESS, OK, API_VERSION));
        } catch (ServiceException e) {
            log.error("@@@ Error al insertar Notificacion con Reporte: {}", e.toString());
            return ResponseEntity.ok(new ResponseData<>(null, FAILED, KO, API_VERSION));
        }
    }

    @DeleteMapping("notificacion-reporte/{notificacionId}/{reporteId}")
    public ResponseEntity<?> eliminarNotificacionReporte(@PathVariable Integer notificacionId,
                                                         @PathVariable Integer reporteId) {
        try {
            int val = notificacionService.deleteNotificacionReporte(notificacionId, reporteId);
            return ResponseEntity.ok(new ResponseData<>(val, SUCCESS, OK, API_VERSION));
        } catch (ServiceException e) {
            log.error("@@@ Error al eliminar Notificacion con Reporte: {}", e.toString());
            return ResponseEntity.ok(new ResponseData<>(null, FAILED, KO, API_VERSION));
        }
    }

    @PostMapping("{notificacionId}/enviar-notificacion")
    public ResponseEntity<?> enviarNotificacion(@PathVariable Integer notificacionId) {
        try {
            boolean enviado = notificacionService.sendNotificacion(notificacionId);
            return ResponseEntity.ok(new ResponseData<>(enviado, SUCCESS, OK, API_VERSION));
        } catch (ServiceException e) {
            log.error("@@@ Error al enviar Notificacion: {}", e.toString());
            return ResponseEntity.ok(new ResponseData<>(false, FAILED, KO, API_VERSION));
        }
    }
}
