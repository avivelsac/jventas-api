package pe.com.avivel.sistemas.jventas.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity(name = "ReporteParametro")
@Table(name = "reporte_parametro")
public class ReporteParametro implements Serializable {

    @Id
    @Column(name = "reporte_parametro_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer reporteParametroId;

    @Column(name = "reporte_parametro_key")
    private String reporteParametroKey;

    @Column(name = "reporte_parametro_value")
    private String reporteParametroValue;

    @Column(name = "reporte_parametro_use_others")
    private boolean reporteParametroUseOthers;

    @Column(name = "reporte_parametro_info")
    private String reporteParametroInfo;

    @JoinColumn(name = "reporte_id", referencedColumnName = "reporte_id")
    @ManyToOne(optional = false, targetEntity = Reporte.class)
    @JsonIgnore
    private Reporte reporte;
}
