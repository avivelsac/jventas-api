package pe.com.avivel.sistemas.jventas.api.service.inf;

import pe.com.avivel.sistemas.jventas.api.entity.ReporteJasper;
import pe.com.avivel.sistemas.jventas.api.service.generic.GenericService;

public interface ReporteJasperService extends GenericService<ReporteJasper> {
}
