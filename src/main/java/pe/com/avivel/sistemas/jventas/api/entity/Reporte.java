package pe.com.avivel.sistemas.jventas.api.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity(name = "Reporte")
@Table(name = "reporte")
public class Reporte implements Serializable {

    @Id
    @Column(name = "reporte_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer reporteId;

    @Column(name = "reporte_codigo")
    private String reporteCodigo;

    @Column(name = "reporte_descripcion")
    private String reporteDescripcion;

    @Column(name = "reporte_observaciones")
    private String reporteObservaciones;

    @Column(name = "reporte_estado")
    @Enumerated(EnumType.STRING)
    private ReporteEstado reporteEstado;

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "reporte",
            targetEntity = ReporteParametro.class,
            fetch = FetchType.EAGER)
    private List<ReporteParametro> parametros = new ArrayList<>();

    @JoinColumn(name = "reporte_jasper_id", referencedColumnName = "reporte_jasper_id")
    @ManyToOne(targetEntity = ReporteJasper.class)
    private ReporteJasper reporteJasper;

    @Override
    public String toString() {
        return reporteDescripcion;
    }
}
