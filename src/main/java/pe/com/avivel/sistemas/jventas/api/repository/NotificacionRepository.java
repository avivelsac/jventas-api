package pe.com.avivel.sistemas.jventas.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.jventas.api.entity.Notificacion;
import pe.com.avivel.sistemas.jventas.api.entity.NotificacionEstado;
import pe.com.avivel.sistemas.jventas.api.entity.NotificacionStatus;

import java.util.Date;
import java.util.List;

@Repository
public interface NotificacionRepository extends JpaRepository<Notificacion, Integer> {

    List<Notificacion> findAllByNotificacionEstado(NotificacionEstado notificacionEstado);

    @Modifying
    @Query(value =
            "delete from notificacion_reporte " +
                    " where notificacion_id=:notificacionId " +
                    " and reporte_id=:reporteId", nativeQuery = true)
    int deleteNotificacionReporte(Integer notificacionId, Integer reporteId);

    @Modifying
    @Query(value =
            "insert into notificacion_reporte(notificacion_id, reporte_id) " +
                    " values(:notificacionId, :reporteId) ", nativeQuery = true)
    int saveNotificacionReporte(Integer notificacionId, Integer reporteId);

    @Modifying
    @Query(" update Notificacion " +
            " set notificacionCantidadEnviar=:cantidadEnviar, " +
            " notificacionCantidadEnviados=:cantidadEnviados " +
            " where notificacionId=:notificacionId ")
    int updateCantidades(Integer cantidadEnviar, Integer cantidadEnviados, Integer notificacionId);

    @Modifying
    @Query(" update Notificacion " +
            " set notificacionUltimaEjecucion=:nuevaUltimaEjecucion, " +
            " notificacionProximaEjecucion=:nuevaProximaEjecucion " +
            " where notificacionId=:notificacionId ")
    int updateFechasEjecuciones(Date nuevaUltimaEjecucion, Date nuevaProximaEjecucion, Integer notificacionId);

    @Modifying
    @Query(" update Notificacion " +
            " set notificacionUltimaEjecucion=:nuevaUltimaEjecucion " +
            " where notificacionId=:notificacionId ")
    int updateUltimaEjecucion(Date nuevaUltimaEjecucion, Integer notificacionId);

    @Modifying
    @Query(" update Notificacion " +
            " set notificacionStatus=:notificacionStatus " +
            " where notificacionId=:notificacionId ")
    int upateStatusNotificacion(NotificacionStatus notificacionStatus, Integer notificacionId);

    @Modifying
    @Query(" update Notificacion " +
            " set notificacionEstado=:notificacionEstado " +
            " where notificacionId=:notificacionId ")
    int upateEstadoNotificacion(NotificacionEstado notificacionEstado, Integer notificacionId);
}
