package pe.com.avivel.sistemas.jventas.api.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Entity(name = "Cliente")
@Table(name = "cliente")
public class Cliente implements Serializable {

    @Id
    @Column(name = "cliente_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "cliente_tipopersona")
    @Enumerated(EnumType.STRING)
    private TipoPersona tipoPersona;

    @JoinColumn(name = "tipodocumentoidentidad_id", referencedColumnName = "tipodocumentoidentidad_id")
    @ManyToOne(targetEntity = TipoDocumentoIdentidad.class)
    private TipoDocumentoIdentidad tipoDocumentoIdentidad;

    @Column(name = "cliente_numerodocumentoidentidad")
    private String numeroDocumentoIdentidad;

    @Column(name = "cliente_razonsocial")
    private String razonSocial;

    @Column(name = "cliente_direccion")
    private String direccion;

    @Column(name = "cliente_departamento")
    private String departamento;

    @Column(name = "cliente_provincia")
    private String provincia;

    @Column(name = "cliente_distrito")
    private String distrito;

    @Column(name = "cliente_email")
    private String email;

    @JoinColumn(name = "canalventa_id", referencedColumnName = "canalventa_id")
    @ManyToOne(targetEntity = CanalVenta.class)
    private CanalVenta canalVenta;

    @Column(name = "cliente_credito")
    private boolean credito = false;

    @Column(name = "cliente_diascredito")
    private Integer diasCredito = 0;

    @Column(name = "cliente_tipocliente")
    @Enumerated(EnumType.STRING)
    private TipoCliente tipoCliente = TipoCliente.N;

    @Column(name = "cliente_lineacredito")
    private BigDecimal lineaCredito = BigDecimal.ZERO;

    @Override
    public String toString() {
        return razonSocial;
    }
}
