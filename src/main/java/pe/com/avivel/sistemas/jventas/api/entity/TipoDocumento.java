package pe.com.avivel.sistemas.jventas.api.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity(name = "TipoDocumento")
@Table(name = "tipo_documento")
@Access(AccessType.FIELD)
public class TipoDocumento implements Serializable {

    @Id
    @Column(name = "tipodocumento_id")
    private Integer id;
}
