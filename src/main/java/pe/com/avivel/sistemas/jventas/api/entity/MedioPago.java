package pe.com.avivel.sistemas.jventas.api.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity(name = "MedioPago")
@Table(name = "medio_pago")
public class MedioPago implements Serializable {

    @Id
    @Column(name = "mediopago_id")
    private Integer id;
}
