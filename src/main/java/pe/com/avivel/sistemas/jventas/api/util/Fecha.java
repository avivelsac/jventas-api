package pe.com.avivel.sistemas.jventas.api.util;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.YearMonth;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

@SuppressWarnings("unused")
public class Fecha {

    private final static String GMT_5 = "-05:00";

    public static Date stringToDate(String fecha) {
        return ConvertUtil.toDate(fecha);
    }

    public static Date diaMas(Integer sumarDias) {
        return ConvertUtil.addDays(new Date(), sumarDias);
    }

    public static Date diaAnterior() {
        return ConvertUtil.addDays(new Date(), -1);
    }

    public static Date diaSemanaAnterior() {
        return ConvertUtil.addDays(new Date(), -7);
    }

    public static Date horaMas(Integer sumarHoras) {
        return ConvertUtil.addHours(new Date(), sumarHoras);
    }

    public static Date primerDia() {
        YearMonth yearMonth = YearMonth.now();
        LocalDate firstDateOfMonth = yearMonth.atDay(1);
        return Date.from(firstDateOfMonth.atStartOfDay().toInstant(ZoneOffset.of("-05:00")));
    }

    public static Date ultimoDia() {
        YearMonth yearMonth = YearMonth.now();
        LocalDate lastDateOfMonth = yearMonth.atEndOfMonth();
        return Date.from(lastDateOfMonth.atStartOfDay().toInstant(ZoneOffset.of("-05:00")));
    }

    public static String dateToString(Date date) {
        return ConvertUtil.toDate(date);
    }

    public static String toFechaHora(Date date) {
        return ConvertUtil.toDateTime(date);
    }

    public static Integer comparar(Date fecha1, Date fecha2) {
        Integer val = null;

        OffsetDateTime date1 = fecha1.toInstant().atOffset(ZoneOffset.of(GMT_5)).truncatedTo(ChronoUnit.MILLIS);
        OffsetDateTime date2 = fecha2.toInstant().atOffset(ZoneOffset.of(GMT_5)).truncatedTo(ChronoUnit.MILLIS);

        if (date1.isEqual(date2)) val = 0;
        if (date1.isBefore(date2)) val = -1;
        if (date1.isAfter(date2)) val = 1;

        return val;
    }

    public static Integer compararHastaMinutos(Date fecha1, Date fecha2) {
        Integer val = null;

        OffsetDateTime date1 = fecha1.toInstant().atOffset(ZoneOffset.of(GMT_5)).truncatedTo(ChronoUnit.MINUTES);
        OffsetDateTime date2 = fecha2.toInstant().atOffset(ZoneOffset.of(GMT_5)).truncatedTo(ChronoUnit.MINUTES);

        if (date1.isEqual(date2)) val = 0;
        if (date1.isBefore(date2)) val = -1;
        if (date1.isAfter(date2)) val = 1;

        return val;
    }

    public static Date agregarDias(Date fecha, int dias) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.DAY_OF_YEAR, dias);
        return calendar.getTime();
    }
}
