package pe.com.avivel.sistemas.jventas.api.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Entity(name = "ReporteJasper")
@Table(name = "reporte_jasper")
public class ReporteJasper implements Serializable {

    @Id
    @Column(name = "reporte_jasper_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer reporteJasperId;

    @Column(name = "reporte_jasper_nombre")
    private String reporteJasperNombre;

    @Column(name = "reporte_jasper_file")
    private String reporteJasperFile;

    @Column(name = "reporte_jasper_out")
    private String reporteJasperOut;

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "reporteJasper",
            orphanRemoval = true,
            targetEntity = ReporteJasperParam.class,
            fetch = FetchType.LAZY)
    private List<ReporteJasperParam> params;

    @ManyToOne(targetEntity = ReporteJasperSource.class)
    @JoinColumn(name = "reporte_jasper_source_id", referencedColumnName = "reporte_jasper_source_id")
    private ReporteJasperSource reporteJasperSource;
}
