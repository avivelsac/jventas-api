package pe.com.avivel.sistemas.jventas.api.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity(name = "CanalVenta")
@Table(name = "canal_venta")
@Access(AccessType.FIELD)
public class CanalVenta implements Serializable {

    @Id
    @Column(name = "canalventa_id")
    private Integer id;

    @Column(name = "canalventa_descripcion")
    private String descripcion;

    @Column(name = "canalventa_responsable")
    private String responsable;
}
