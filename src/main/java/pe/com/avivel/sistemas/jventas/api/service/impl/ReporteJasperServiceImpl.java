package pe.com.avivel.sistemas.jventas.api.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pe.com.avivel.sistemas.jventas.api.entity.ReporteJasper;
import pe.com.avivel.sistemas.jventas.api.repository.ReporteJasperRepository;
import pe.com.avivel.sistemas.jventas.api.service.exception.ServiceException;
import pe.com.avivel.sistemas.jventas.api.service.inf.ReporteJasperService;

import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class ReporteJasperServiceImpl implements ReporteJasperService {

    private final ReporteJasperRepository reporteJasperRepository;

    @Override
    public ReporteJasper insert(ReporteJasper reporteJasper) throws ServiceException {
        return null;
    }

    @Override
    public ReporteJasper update(ReporteJasper reporteJasper, Integer id) throws ServiceException {
        return null;
    }

    @Override
    public void delete(Integer id) throws ServiceException {

    }

    @Override
    public ReporteJasper findById(Integer id) throws ServiceException {
        return reporteJasperRepository.findById(id).orElse(null);
    }

    @Override
    public List<ReporteJasper> getAll() throws ServiceException {
        return reporteJasperRepository.findAll();
    }
}
