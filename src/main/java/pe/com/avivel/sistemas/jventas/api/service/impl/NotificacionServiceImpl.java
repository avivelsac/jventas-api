package pe.com.avivel.sistemas.jventas.api.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import pe.com.avivel.sistemas.jventas.api.entity.Notificacion;
import pe.com.avivel.sistemas.jventas.api.entity.NotificacionEstado;
import pe.com.avivel.sistemas.jventas.api.entity.NotificacionStatus;
import pe.com.avivel.sistemas.jventas.api.repository.NotificacionRepository;
import pe.com.avivel.sistemas.jventas.api.repository.ReporteRepository;
import pe.com.avivel.sistemas.jventas.api.service.EmailSenderService;
import pe.com.avivel.sistemas.jventas.api.service.exception.ServiceException;
import pe.com.avivel.sistemas.jventas.api.service.inf.NotificacionService;
import pe.com.avivel.sistemas.jventas.api.util.Fecha;
import pe.com.avivel.sistemas.jventas.api.util.Texto;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class NotificacionServiceImpl implements NotificacionService {

    private final EmailSenderService emailSenderService;
    private final NotificacionRepository notificacionRepository;
    private final ReporteRepository reporteRepository;

    @Override
    public Notificacion insert(Notificacion notificacion) throws ServiceException {
        notificacion.setNotificacionProximaEjecucion(getProximaEjecucion(notificacion.getNotificacionPrimeraEjecucion()));
        notificacion.setNotificacionStatus(NotificacionStatus.ESPERA);
        notificacion.setNotificacionEstado(NotificacionEstado.ACTIVO);

        return notificacionRepository.save(notificacion);
    }

    @Override
    public Notificacion update(Notificacion notificacion, Integer id) {
        if (notificacionRepository.findById(id).isPresent()) {
            Notificacion fetched = notificacionRepository.findById(id).get();
            BeanUtils.copyProperties(notificacion, fetched, Texto.ignoreNullsFrom(notificacion));
            fetched.setNotificacionProximaEjecucion(getProximaEjecucion(notificacion.getNotificacionPrimeraEjecucion()));
            return notificacionRepository.save(fetched);
        }
        return null;
    }

    private Date getProximaEjecucion(Date fecha) {
        Date proximaEjecucion;
        if (Fecha.compararHastaMinutos(new Date(), fecha) == -1) {
            proximaEjecucion = fecha;
        } else {
            proximaEjecucion = Fecha.agregarDias(fecha, 1);
        }
        return proximaEjecucion;
    }

    @Override
    public void delete(Integer id) {
        notificacionRepository.upateEstadoNotificacion(NotificacionEstado.ELIMINADO, id);
    }

    @Override
    public Notificacion findById(Integer id) {
        Notificacion notificacion = notificacionRepository.getById(id);
        notificacion.setReportes(reporteRepository.findAllByNotificacionId(id));
        return notificacion;
    }

    @Override
    public List<Notificacion> getAll() {
        List<Notificacion> notificaciones = notificacionRepository.findAll();
        notificaciones.forEach((Notificacion n) -> n.setReportes(reporteRepository.findAllByNotificacionId(n.getNotificacionId())));
        return notificaciones;
    }

    @Override
    public List<Notificacion> getNotificacionesByEstado(NotificacionEstado notificacionEstado) {
        List<Notificacion> notificaciones = notificacionRepository.findAllByNotificacionEstado(notificacionEstado);
        notificaciones.forEach((Notificacion n) -> n.setReportes(reporteRepository.findAllByNotificacionId(n.getNotificacionId())));
        return notificaciones;
    }

    @Override
    public int saveNotificacionReporte(Integer notificacionId, Integer reporteId) {
        return notificacionRepository.saveNotificacionReporte(notificacionId, reporteId);
    }

    @Override
    public int deleteNotificacionReporte(Integer notificacionId, Integer reporteId) {
        return notificacionRepository.deleteNotificacionReporte(notificacionId, reporteId);
    }

    @Override
    public boolean updateCantidades(Integer cantidadEnviar, Integer cantidadEnviados, Integer notificacionId) {
        return notificacionRepository.updateCantidades(cantidadEnviar, cantidadEnviados, notificacionId) > 0;
    }

    @Override
    public boolean updateFechasEjecuciones(Date nuevaUltimaEjecucion, Date nuevaProximaEjecucion, Integer notificacionId) {
        return notificacionRepository.updateFechasEjecuciones(nuevaUltimaEjecucion, nuevaProximaEjecucion, notificacionId) > 0;
    }

    @Override
    public void updateStatus(NotificacionStatus notificacionStatus, Integer notificacionId) {
        notificacionRepository.upateStatusNotificacion(notificacionStatus, notificacionId);
    }

    @Override
    public boolean sendNotificacion(Integer notificacionId) {
        Notificacion notificacion = this.findById(notificacionId);
        return emailSenderService.sendNotificacion(notificacion, false);
    }
}
