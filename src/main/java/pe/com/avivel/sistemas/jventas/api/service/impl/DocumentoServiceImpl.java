package pe.com.avivel.sistemas.jventas.api.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.avivel.sistemas.jventas.api.entity.Articulo;
import pe.com.avivel.sistemas.jventas.api.entity.Cliente;
import pe.com.avivel.sistemas.jventas.api.entity.DetalleDocumento;
import pe.com.avivel.sistemas.jventas.api.entity.Documento;
import pe.com.avivel.sistemas.jventas.api.repository.ArticuloRepository;
import pe.com.avivel.sistemas.jventas.api.repository.ClienteRepository;
import pe.com.avivel.sistemas.jventas.api.repository.DocumentoRepository;
import pe.com.avivel.sistemas.jventas.api.service.exception.NotFoundException;
import pe.com.avivel.sistemas.jventas.api.service.exception.ServiceException;
import pe.com.avivel.sistemas.jventas.api.service.inf.DocumentoService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional
public class DocumentoServiceImpl implements DocumentoService {

    private final DocumentoRepository documentoRepository;
    private final ClienteRepository clienteRepository;
    private final ArticuloRepository articuloRepository;

    @Autowired
    public DocumentoServiceImpl(DocumentoRepository documentoRepository, ClienteRepository clienteRepository, ArticuloRepository articuloRepository) {
        this.documentoRepository = documentoRepository;
        this.clienteRepository = clienteRepository;
        this.articuloRepository = articuloRepository;
    }

    @Override
    public Documento insert(Documento documento) throws ServiceException {
        return null;
    }

    @Override
    public Documento update(Documento documento, Integer id) throws ServiceException {
        return null;
    }

    @Override
    public void delete(Integer id) throws ServiceException {

    }

    @Override
    public Documento findById(Integer id) throws ServiceException {
        Optional<Documento> documento = documentoRepository.findById(id);
        if (documento.isPresent()) {
            return documento.get();
        } else {

            log.error("### JVENTAS-API: findById: No se pudo encontrar el Documento con id: {}", id);
            throw new ServiceException("No se pudo encontrar el Documento con id: " + id);
        }
    }

    @Override
    public List<Documento> getAll() throws ServiceException {
        return null;
    }

    @Override
    @Transactional
    public Documento create(Documento documento) throws ServiceException, NotFoundException {
        log.info("### Procesando Documento {} -> ", documento);

        Documento fetchedDocumento = documentoRepository.getDocumentoByTipoAndSerieAndNumero(
                documento.getTipoDocumento().getId(),
                documento.getSerie(),
                documento.getNumero()
        );

        if (fetchedDocumento == null) {

            // Crear y/o Asignar Cliente (Cliente en Comprobante)
            this.createOrAsignCliente(documento.getCliente(), documento);

            // Crear y/o Asignar Cliente Destino (Cliente Real)
            this.createOrAsignClienteDestino(documento.getClienteDestino(), documento);

            // Crear y/o Asignar Detalles
            this.createOrAsignDetalles(documento);

            // Crear Documento
            Documento documentoSaved = documentoRepository.save(documento);

            log.info("### Documento procesado satisfactoriamente -> {}", documentoSaved);

            return documentoSaved;
        } else {

            log.info("### El Documento/Comprobante {} ya existe.", documento);
            return fetchedDocumento;
        }
    }

    private void createOrAsignCliente(Cliente cliente, Documento documento) throws NotFoundException {
        if (cliente != null) {

            Cliente existingCliente = clienteRepository.findClienteByNumeroDocumentoIdentidad(cliente.getNumeroDocumentoIdentidad());
            if (existingCliente == null) {

                Cliente savedCliente = clienteRepository.save(cliente);
                documento.setCliente(savedCliente);
            } else {

                documento.setCliente(existingCliente);
            }
        } else {

            log.error("### JVENTAS-API: createOrAsignCliente: La Entidad no contiene informacion sobre el Cliente: {}", documento);
            throw new NotFoundException("La Entidad no contiene informacion sobre el Cliente");
        }
    }

    private void createOrAsignClienteDestino(Cliente cliente, Documento documento) throws NotFoundException {
        if (cliente != null) {

            Cliente existingCliente = clienteRepository.findClienteByNumeroDocumentoIdentidad(cliente.getNumeroDocumentoIdentidad());
            if (existingCliente == null) {

                Cliente savedCliente = clienteRepository.save(cliente);
                documento.setClienteDestino(savedCliente);
            } else {

                documento.setClienteDestino(existingCliente);
            }
        } else {

            log.error("### JVENTAS-API: createOrAsignClienteDestino: La Entidad no contiene informacion sobre el Cliente Destino: {}", documento);
            throw new NotFoundException("La Entidad no contiene informacion sobre el Cliente Destino");
        }
    }

    private void createOrAsignDetalles(Documento documento) throws NotFoundException {
        for (DetalleDocumento dc : documento.getDetalleDocumentoList()) {

            Articulo articulo = dc.getArticulo();
            if (articulo != null) {

                String codigo = articulo.getCodigo();

                Articulo fetchedArticulo = articuloRepository.findByCodigo(codigo);
                if (fetchedArticulo != null) {

                    dc.setArticulo(fetchedArticulo);
                } else {

                    log.error("### JVENTAS-API: createOrAsignDetalles: El Detalle no contiene informacion sobre el Articulo: {}", documento);
                    throw new NotFoundException("El Detalle no contiene informacion sobre el Articulo.");
                }

            } else {

                log.error("### JVENTAS-API: createOrAsignDetalles: El Detalle no contiene informacion sobre el Articulo Detalle: {}", documento);
                throw new NotFoundException("El Detalle no contiene informacion sobre el Articulo Detalle.");
            }

            dc.setDocumento(documento);
        }
    }
}
