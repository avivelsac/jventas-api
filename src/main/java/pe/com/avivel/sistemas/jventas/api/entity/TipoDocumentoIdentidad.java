package pe.com.avivel.sistemas.jventas.api.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity(name = "TipoDocumentoIdentidad")
@Table(name = "tipo_documento_identidad")
public class TipoDocumentoIdentidad implements Serializable {

    @Id
    @Column(name = "tipodocumentoidentidad_id")
    private Integer id;
}
